;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: com.symsim.oss.lispm; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(in-package :com.symsim.oss.lispm)


;;
;; Top-level.  With apologies to Debbie Harry, call me!
;;

(defun load-lispm-init-if-any ()
  (let ((lispm-init-path (merge-pathnames
			  (user-homedir-pathname)
			  (make-pathname
			   :name "lispm-init"
			   :type "lisp"))))
    (when (probe-file lispm-init-path)
      (format *terminal-io* "Loading ~S~%" lispm-init-path)
      (load lispm-init-path))))

(defun run ()
  ;; (clim-debugger::enable-clim-debugger)
  (load-lispm-init-if-any)
  (clim-sys:make-process (lambda () (clim-listener:run-listener)))
  #+sbcl (sb-impl::toplevel-repl nil)
  #+ccl (ccl::listener-function))
