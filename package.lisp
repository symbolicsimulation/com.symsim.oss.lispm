;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: cl-user; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(defpackage #:com.symsim.oss.lispm
  (:use :clim :clim-extensions :clim-lisp)
  (:documentation
   "The Good Old Days."))

