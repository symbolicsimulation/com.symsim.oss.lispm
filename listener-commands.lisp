;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: clim-listener; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;
;;; Probably Poor Form (see what I did there?) to put these into the
;;; clim-listener package, but I tried to subclass the listener
;;; application-frame and install such commands on the derived frame.
;;; No dice.
;;;

(in-package :clim-listener)

(defun find-or-spawn (type-to-match list-of-args-to-make-process)
  (let ((frame-manager (find-frame-manager)))
    (when frame-manager
      (let ((f (find-if (lambda (x) (typep x type-to-match)) (frame-manager-frames frame-manager))))
	(cond ((null f)
	       (apply 'clim-sys:make-process list-of-args-to-make-process))
	      (t
	       (raise-frame f)))))))

;;
;; Listener commands for well-defined application frames.  Intent is
;; to have single instances of such application frames so as to ape
;; the Lisp Machine (where SELECT-E brought up the sole Zmacs).
;;

(define-listener-command (com-demodemo :menu nil :name "McCLIM Demodemo")
  ()
  (find-or-spawn
   'clim-demo::demodemo
   (list #'clim-demo::demodemo :name "McCLIM Demos")))

(define-listener-command (com-quicklisp-gui :menu nil :name "Quicklisp GUI")
  ()
  (find-or-spawn
   'com.symsim.oss.ql-gui::quicklisp-gui
   (list
    (lambda () (clim:run-frame-top-level (clim:make-application-frame 'com.symsim.oss.ql-gui::quicklisp-gui)))
    :name "Quicklisp")))

(define-listener-command (com-imap-mail :menu nil :name "Mail (IMAP)")
  ()
  (find-or-spawn
   'com.symsim.oss.lispm::imap-mail
   (list
    (lambda () (clim:run-frame-top-level (clim:make-application-frame 'com.symsim.oss.lispm::imap-mail)))
    :name "Mail Client (IMAP)")))

(define-listener-command (com-filesystem-browser :menu nil :name "Filesystem Browser")
  ()
  (find-or-spawn
   'clim-treelist.filesystem-viewer::filesystem-viewer
   (list
    (lambda () (clim-treelist.filesystem-viewer:browse-files #P"/"))
    :name "Filesystem Browser")))

(define-listener-command (com-foo :menu nil :name "Foo")
  ()
  (clim:run-frame-top-level (clim:make-application-frame 'com.symsim.oss.lispm::quicklisp-gui)))

(define-listener-command (com-peek :menu nil :name "Peek")
  ()
  (find-or-spawn
   'com.symsim.oss.lispm::peek-gui
   (list
    (lambda () (clim:run-frame-top-level (clim:make-application-frame 'com.symsim.oss.lispm::peek-gui)))
    :name "Filesystem Browser")))

(define-listener-command (com-inspector :menu nil :name "Inspector")
  ()
  (clouseau:inspect :new-process t))

(define-listener-command (com-inspect-application-frame :menu nil :name "Inspect Application Frame")
  ()
  (clouseau:inspect *application-frame* :new-process t))

(define-listener-command (com-climacs :menu nil :name "Editor (Climacs)")
  ()
  (climacs::edit-file nil))

(define-listener-command (com-mel-imap-test :menu nil :name "Mel IMAP Test")
  ()
  (let* ((imap (mel:make-imaps-folder :host "192.168.0.1" :username "jm" :password ""))
	 (messages (mel:messages imap)))
    (dolist (message messages)
      (format *terminal-io* "~A ~A~%" (mel:from message) (mel:subject message)))))

(define-listener-command (com-mel-smtp-test :menu nil :name "Mel SMTP Test")
  ()
  (let ((new-message (mel:make-message :subject "Test Message"
				       :from "jm"
				       :to "jm"
				       :body "My body"))
	(smtp (mel:make-smtp-relay-folder :host "192.168.0.1"
					  :username "jm"
					  :password "good+dog")))
    (clouseau:inspect (mel:copy-message new-message smtp))))

;;
;; Install the commands into a single command table.  Eventually,
;; these should be keystrokes intercepted by a window manager derived
;; off of the Common Lisp version of TinyWM.
;;

(make-command-table 'lispm-command-table
		    :errorp nil
		    :menu '(("Editor (Climacs)" :command com-climacs)
			    ("Filesystem Browser" :command com-filesystem-browser)
			    ("Inspector" :command com-inspector)
			    ("Mel IMAP Test" :command com-mel-imap-test)
			    ("Mail (IMAP)" :command com-imap-mail)
			    ("Peek" :command com-peek)

			    ("Quicklisp GUI" :command com-quicklisp-gui)
			    ("Demos (McCLIM)" :command com-demodemo)

			    ("Inspect Application Frame" :command com-inspect-application-frame)
			    ("Mel SMTP Test" :command com-mel-smtp-test)
			    ("Foo" :command com-foo)
			    )
		    )

;;
;; Install the command table into the listener.
;;

(defmethod initialize-instance :after ((self listener) &rest args)
  (add-menu-item-to-command-table 'listener "SELECT-able Frames" :menu 'lispm-command-table))
