;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: com.symsim.oss.lispm; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(in-package :com.symsim.oss.lispm)

;;
;; Presentation type for only for Quicklisp's "system" object.
;;

(define-presentation-type ql-dist:system ())

;; Writes name of system in +sea-green+ if installed, black if not.

(define-presentation-method present (object (type ql-dist:system) stream view &key)
  (declare (ignore view))
  (let (ink)
    (if (ql-dist::installedp object)
	(setf ink +sea-green+)
      (setf ink +black+))
  (with-drawing-options
   (stream :ink ink)
   (write-string (ql-dist::name object) stream))))

;; Enables textual input of system names, with suggestions.

(define-presentation-method accept ((type ql-dist::system) stream view &key)
  (multiple-value-bind
   (object success str)
   (completing-from-suggestions (stream)
				(dolist (system (ql-dist::provided-systems t))
				  (suggest (ql::name system) system)))
   (if success
       (values object 'ql-dist::system)
     (simple-parse-error (format nil "No system available with name ~S" str)))))

;;
;; The application frame itself, along with its single display-function.
;;

(define-application-frame quicklisp-gui ()
;  ((foo :initform nil))
  ()
  (:name "A Simple GUI for Quicklisp")
  (:pointer-documentation t)
  (:panes
   (interactor :interactor)
   (system-list :application
		:incremental-redisplay t
		:display-function 'display-systems))
  (:layouts
   (default
     (vertically (:width 800)
		 (2/3 (labelling (:label "Quicklisp List of Available Systems") system-list))
		 (1/3 (labelling (:label "Interactor") interactor))))))
			
(defmethod display-systems ((frame quicklisp-gui) stream)
  (dolist (system (ql-dist::provided-systems t))
    (updating-output (stream :unique-id system)
		     (present system 'ql-dist:system :stream stream)
		     (terpri stream)))
  (setf (window-viewport-position stream) (values 0 0)))

(defmethod frame-standard-output ((frame quicklisp-gui))
  (get-frame-pane frame 'interactor))

;;
;; The application frame's various (mostly Quicklisp) commands.
;;

(define-quicklisp-gui-command (com-quit-quicklisp-gui
			:menu "Quit"
			:name "Quit")
  ()
  (frame-exit *application-frame*))

#|
(define-quicklisp-gui-command (com-add-to-init-file :menu "Add to Init File")
  ()
  (format *terminal-io* "com-add-to-init-file~%"))
|#

(define-quicklisp-gui-command (com-system-apropos
			:menu "System-Apropos"
			:name "System Apropos")
  ((name 'string))
  (dolist (system (ql-dist::provided-systems t))
    (when (search name (ql-dist::name system))
      (present system 'ql-dist:system)
      (terpri))))

(define-quicklisp-gui-command (com-update-all-dists
			:menu "Update-All-Installed-Software"
			:name "Update All Installed Software")
  ()
  (format t "Calling (ql:update-all-dists)~%")
  (ql:update-all-dists)
  (redisplay-frame-pane *application-frame* (find-pane-named *application-frame* 'system-list)))

(define-quicklisp-gui-command (com-update-client
			:menu "Update-Quicklisp-Client"
			:name "Update Quicklisp Client")
  ()
  (format t "Calling (ql:update-client)~%")
  (ql:update-client)
  (redisplay-frame-pane *application-frame* (find-pane-named *application-frame* 'system-list)))

(define-quicklisp-gui-command (com-install-system :name "Install System")
  ((system 'ql-dist:system
	   :gesture :select
	   :gesture :menu))
  (cond ((ql-dist::installedp system)
	 (format t "System ")
	 (present system 'ql-dist:system)
	 (format t " already installed~%"))
	(t
	 (format t "Going to (ql:quickload ~S)~%" (ql-dist::name system))
	 (ql:quickload (ql-dist::name system)))))

;; Quicklisp uninstall implementation is in-process as of this writing.

(define-quicklisp-gui-command (com-uninstall-system :name "Uninstall System")
  ((system 'ql-dist:system
	   :gesture :delete
	   :gesture :menu))
  (cond ((ql-dist::installedp system)
	 (format t "Going to (ql:uninstall ~S)~%" (ql-dist::name system))
	 (ql::uninstall (ql-dist::name system))
	 (format t "Done.~%"))
	(t
	 (format t "System ")
	 (present system 'ql-dist:system)
	 (format t " not installed~%"))))

;; This redraws the list of all quicklisp systems.

(define-quicklisp-gui-command (com-refresh
			:menu "Refresh-List-of-Systems"
			:name "Refresh List of Systems")
  ()
  (redisplay-frame-pane *application-frame* (find-pane-named *application-frame* 'system-list)))

;;
;; Testing cruft.
;;

#|
(define-quicklisp-gui-command (com-foo :menu "Foo" :name "Foo")
  ()
;  (clouseau:inspect *standard-output*)
  (clouseau:inspect (find-command-table 'lispm-cmds))
  )
|#

(define-quicklisp-gui-command (com-inspect-system :name "Inspect System")
  ((system 'ql-dist:system
	   :gesture :edit
	   :gesture :menu))
  (clouseau:inspect system))
