;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: com.symsim.oss.lispm.asd; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;
;;; First   (load "com.symsim.oss.lispm.asd")
;;; then    (asdf:oos 'asdf:load-op :com.symsim.oss.lispm)
;;; finally (com.symsim.oss.lispm::run)
;;;

(defpackage #:com.symsim.oss.lispm.asd
  (:use :cl :asdf))

(in-package :com.symsim.oss.lispm.asd)

(defsystem com.symsim.oss.lispm
    :name "com.symsim.oss.lispm"
    :version "0.0.1"
    :maintainer "jm@symbolic-simulation.com"
    :author "jm@symbolic-simulation.com"
    :licence "Golden Rule License"
    :description "Because I want to, not because I have to."
    :depends-on
    (:mcclim
     ;; #-CCL :mcclim-truetype
     :clim-examples		      ; McCLIM demos
     :com.symsim.oss.ql-gui	      ; Quicklisp GUI
     :clim-listener		      ; Lisp Listener
     :clouseau			      ; Inspector
     :climacs			      ; Editor (Common Lisp eMACS)
     :clim-treelist		      ; Filesystem browser
     :mel-base			      ; Email, su
     :clim-debugger		      ; From my separate repo, as
					; McCLIM lacks an ASD file for
					; its debugger
     :trivial-dump-core			; For dumping images, etc.
     #-ccl :dbus				; Realize this is *nix-specific.
     )
    :components (
		 (:file "package")
		 (:file "init" :depends-on ("package"))
		 (:file "frame" :depends-on ("package"))
		 (:file "mail" :depends-on ("package"))
		 (:file "peek" :depends-on ("package"))
		 (:file "listener-commands")
		 )
    )
