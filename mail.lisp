;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: com.symsim.oss.lispm; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(in-package :com.symsim.oss.lispm)

(defclass messages-view (view) ())
(defparameter +messages-view+ (make-instance 'messages-view))
(defclass message-view (view) ())
(defparameter +message-view+ (make-instance 'message-view))

(define-presentation-type mel.mime:message ())

(define-presentation-method present (object (type mel.mime:message) stream (view messages-view) &key)
  #+NIL (write-string (mel:subject object) stream)
  #+NIL (terpri stream)
  #-NIL (format
	 stream
	 "UID ~A: Date: ~A: From: ~A: Subject; ~A~%"
	 (mel:uid object)
	 (mel:date object)
	 (mel:from object)
	 (mel:subject object))
  )


(defvar *imap-mailbox* nil) ; Set to some return value of mel:make-imap-folder in lispm-init

(define-application-frame imap-mail ()
  ((imap-server :initform *imap-mailbox*)
   (current-message :initform nil))
  (:name "A Simple GUI for IMAP")
  (:pointer-documentation t)
  (:panes
   (messages
    :application
    :incremental-redisplay t
    :default-view +messages-view+
    :display-function 'display-messages)
   (current-message
    :application
    :incremental-redisplay t
    :default-view +message-view+
    :display-function 'display-current-message))
  (:layouts
   (default
     (vertically (:width 800)
		 (1/4 (labelling (:label "Messages") messages))
		 (3/4 (labelling (:label "Current Message") current-message))))))

(defmethod display-messages ((frame imap-mail) stream)
  (when (slot-value frame 'imap-server)
    (dolist (msg (mel:messages (slot-value frame 'imap-server)))
      (present msg 'mel.mime:message :stream stream))))

(defmethod display-current-message ((frame imap-mail) stream)
  (let ((msg (slot-value frame 'current-message)))
    (when msg
      (format stream "BODY~%"))))

(define-imap-mail-command (com-inspect :name "Inspect" :menu t :name "Inspect")
  ()
  (clouseau:inspect *application-frame*))
  

(define-imap-mail-command (com-select-message :name "Select Message")
  ((msg 'mel.mime:message
	:gesture :select
	:gesture :menu))
  (setf (slot-value *application-frame* 'current-message) msg))
