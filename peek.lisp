;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: cl-user; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;

(in-package :com.symsim.oss.lispm)

;(trace clim:frame-current-layout)
;(trace clim:layout-frame)

;(trace clim:generate-panes)

;(trace clim:sheet-disown-child)
;(trace clim:sheet-adopt-child)

;(trace xlib:map-window)
;(trace adopt-frame)

;(trace realize-mirror-aux)

;(trace port-enable-sheet)
;(trace clim:resize-sheet)
;(trace clim:move-and-resize-sheet)

;(trace clim:sheet-transformation :methods t)

;;
;; Command tables
;;

#|
(make-command-table 'quit-commands
		    :errorp nil
		    :menu '(("Program" :command com-quit-program)
			    ("Lisp" :command com-quit-lisp)))

(make-command-table 'update-commands
		    :errorp nil
		    :menu '(("Client" :command com-update-client)
			    ("Dist" :command com-update-dist)
			    ("All Dists" :command com-update-all-dists)))

(make-command-table 'peek-menu-bar
		    :errorp nil
		    :menu '(("Quit" :menu quit-commands)
			    ("Install" :menu install-commands)
			    ("Update" :menu update-commands)
			    ("Uninstall" :menu uninstall-commands)
			    ("Apropos" :command com-system-apropos)
			    ("Refresh" :command com-refresh)))
|#		    
			    

;;
;; The application frame itself, along with its single display-function.
;;

(define-application-frame peek-gui ()
  ((foo :initform nil))
  (:name "Peek")
  ;; (:menu-bar peek-menu-bar)
  ;; (:menu-bar t)
  (:pointer-documentation t)
  (:panes
   (processes
    :application
    :incremental-redisplay t
    :width 1000 :height 1000
    :max-width +fill+ :height +fill+
    :display-function 'display-processes)
   (areas
    :application
    :incremental-redisplay t
    :width 1000 :height 1000
    :max-width +fill+ :height +fill+
    :display-function 'display-areas)
   (meters
    :application
    :incremental-redisplay t
    :width 1000 :height 1000
    :max-width +fill+ :height +fill+
    :display-function 'display-meters)
   (file-system
    :application
    :incremental-redisplay t
    :width 1000 :height 1000
    :max-width +fill+ :height +fill+
    :display-function 'display-file-system)
   (windows
    :application
    :incremental-redisplay t
    :width 1000 :height 1000
    :max-width +fill+ :height +fill+
    :display-function 'display-windows)
   (servers
    :application
    :incremental-redisplay t
    :width 1000 :height 1000
    :max-width +fill+ :height +fill+
    :display-function 'display-servers)
   (network
    :application
    :incremental-redisplay t
    :width 1000 :height 1000
    :max-width +fill+ :height +fill+
    :display-function 'display-network)
   (help
    :application
    :incremental-redisplay t
    :width 1000 :height 1000
    :max-width +fill+ :height +fill+
    :display-function 'display-help)
   (quit
    :application
    :incremental-redisplay t
    :width 1000 :height 1000
    :max-width +fill+ :height +fill+
    :display-function 'display-quit)
   (hostat
    :application
    :incremental-redisplay t
    :width 1000 :height 1000
    :max-width +fill+ :height +fill+
    :display-function 'display-hostat)
   )
  (:layouts
   (processes processes)
   (areas-layout areas)
   (meters-layout meters)
   (file-system-layout file-system)
   (windows-layout windows)
   (servers-layout servers)
   (network-layout network)
   (hostat-layout hostat)))

;;
;; Display functions
;;

(defmethod display-processes ((frame peek-gui) stream)
  (dolist (process (clim-sys:all-processes))
    (write process :stream stream)
    (terpri stream)))

(defmethod display-areas ((frame peek-gui) stream)
  (let ((*standard-output* (find-pane-named *application-frame* 'areas)))
    (room)))

(defmethod display-meters ((frame peek-gui) stream)
  (write "meters TBD" :stream stream))

(defmethod display-file-system ((frame peek-gui) stream)
  (write "filesystem TBD (should I use clim-treelist browser?)" :stream stream))

(defmethod display-windows ((frame peek-gui) stream)
  (dolist (frame (frame-manager-frames (find-frame-manager)))
    (write frame :stream stream)
    (terpri stream)))

(defmethod display-servers ((frame peek-gui) stream)
  (write "servers TBD" :stream stream))

(defmethod display-network ((frame peek-gui) stream)
  (write "network TBD" :stream stream))

(defmethod display-help ((frame peek-gui) stream)
  (write "help TBD" :stream stream))

(defmethod display-quit ((frame peek-gui) stream)
  (write "quit TBD" :stream stream))

(defmethod display-hostat ((frame peek-gui) stream)
  (write "hostat TBD" :stream stream))

#|

;;
;; Ensure output goes to the interactor pane
;;

(defmethod frame-standard-output ((frame peek-gui))
  (get-frame-pane frame 'interactor))

|#

;;
;; Commands
;;

(define-peek-gui-command (com-inspect :menu "Inspect" :name "Inspect")
  ()
  (clouseau:inspect *application-frame*))

(define-peek-gui-command (com-processes :menu "Processes" :name "Processes")
  ()
;  (clouseau:inspect (list :graft (find-graft)))
  (clouseau:inspect (list :sheet (frame-top-level-sheet *application-frame*)))
  (setf (frame-current-layout *application-frame*) 'processes)
  (clim:move-and-resize-sheet (frame-top-level-sheet *application-frame*) 0 0 400 400)
#|
  (layout-frame *application-frame*)
  (redisplay-frame-pane *application-frame* (find-pane-named *application-frame* 'processes) :force-p t)
  (redisplay-frame-panes *application-frame* :force-p t)
|#
  )

(define-peek-gui-command (com-areas :menu "Areas" :name "Areas")
  ()
(setf (frame-current-layout *application-frame*) 'areas-layout))

(define-peek-gui-command (com-meters :menu "Meters" :name "Meters")
  ()
(setf (frame-current-layout *application-frame*) 'meters-layout))

(define-peek-gui-command (com-windows :menu "Windows" :name "Windows")
  ()
(setf (frame-current-layout *application-frame*) 'windows-layout))

(define-peek-gui-command (com-servers :menu "Servers" :name "Servers")
  ()
(setf (frame-current-layout *application-frame*) 'servers-layout))

(define-peek-gui-command (com-network :menu "Network" :name "Network")
  ()
(setf (frame-current-layout *application-frame*) 'network-layout))

(define-peek-gui-command (com-hostat :menu "Hostat" :name "Hostat")
  ()
(setf (frame-current-layout *application-frame*) 'hostat-layout))

;;
;; Commands that do not change layout
;;

(define-peek-gui-command (com-quit-peek-gui :menu "Quit" :name "Quit")
  ()
(frame-exit *application-frame*))

(define-peek-gui-command (com-help :menu "Help" :name "Help")
  ()
  (format *terminal-io* "com-quit-peek-gui NO-OP~%"))
