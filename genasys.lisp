;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: cl-user; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012-2014.
;;; 
;;; License "Golden Rule License"
;;;

(ql:quickload :com.symsim.oss.lispm)

(trivial-dump-core:save-executable
 #+ccl "lispm.ccl"
 #+sbcl "lispm.sbcl"
 'com.symsim.oss.lispm::run)
